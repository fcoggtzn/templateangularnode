import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HolaMundoService {

  private uri = environment.api  ;
  
  constructor(private http: HttpClient) { 
    console.log(environment.api)
  }

  getAll() {
    return this.http.get(`${this.uri}/`, { observe: 'response' });
  }
/*
  update(id,obj) {
    return this.http.patch(`${this.uri}/${id}`, obj, { observe: 'response' })
  }
  
  get(id) {
    return this.http.get(`${this.uri}/${id}`)
  }

  validar(uuid) {
    return this.http.get(`${this.engine_uri}?uuid=${uuid}`, 
    { observe: 'response' })

  }*/
}
