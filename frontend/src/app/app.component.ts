import { Component , OnInit} from '@angular/core';
import { HolaMundoService } from './hola-mundo.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  datos = [];

  constructor(public servicioApi:HolaMundoService){}


  ngOnInit() {
  this.servicioApi.getAll()
  .subscribe((data: any) => {
    console.log(data);
    this.datos = data.body;
    console.log(this.datos);
  //  this.comprobanteResponse = data.body;
  })
}

}
