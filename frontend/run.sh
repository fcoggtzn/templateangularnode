#!/usr/bin/env bash

# Get path for THIS script
pushd $(dirname $0) > /dev/null
SCRIPT_PATH=$(pwd -P)
popd > /dev/null

cd ${SCRIPT_PATH}

APP_PATH=${SCRIPT_PATH}




docker run -d \
    --name test-webapp \
    -p 480:80 \
    -v ${APP_PATH} \
    test-webapp

#
#sleep  10
#docker cp default.conf test-webapp:/etc/nginx/conf.d/default.conf 
#docker stop evpl-webapp
#sleep  10
#docker start evpl-webapp
